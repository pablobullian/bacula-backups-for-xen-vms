bacula-scripts-for-xen
======================

a series of bacula pre and post run to deal with VMs on Xen


USAGE:
======

It consist of two scripts, one that shutdowns the VM and compress it, another one that brings it up again once Bacula finished the copy.
On the Bacula director server you have to add a job like this one (pay attention on how to call this two scripts) :


```
#!config

Job {
  Name = "Backup_imagen"
  JobDefs = "back_imagen"
  Type = Backup  
RunScript {
    RunsWhen = Before
    FailJobOnError = yes
    Command = "/etc/bacula/scripts/prerun_image.sh IP VM_NAME VOLUME DISK_NAME SSH_USER"
}

FileSet = "Set imagen"
RunScript {
    RunsWhen = After
    FailJobOnError = yes
    Command = "/etc/bacula/scripts/postrun_image.sh IP VM_NAME VOLUME DISK_NAME VM_CREATION_CONFIG_FILE"
}
  Write Bootstrap = "/var/lib/bacula/%n.bsr"
  Priority = 11                   # run after main backup
}
```
